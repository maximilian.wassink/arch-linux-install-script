#!/bin/bash

read -p "Do you want to use a Swap-Partition (type 1 for yes)? " swap_on

if [ "$swap_on" = "1" ]; then
  read -p "Please enter your Swap-Partition (Type: Swap): /dev/" r_swap
  swap="/dev/$r_swap"
  mkswap -L swap $swap
  swapon -L swap
fi