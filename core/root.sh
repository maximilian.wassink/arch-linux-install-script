#!/bin/bash

read efi
read device
read password
read boot

locale-gen
ln -sf /usr/share/zoneinfo/Europe/Berlin /etc/localtime
mkinitcpio -p linux
(echo "$password"; echo "$password"; ) | passwd

if [ "$efi" = 1 ]; then
  grub-install --target=x86_64-efi --efi-directory=/boot --bootloader-id=$boot
else
  grub-install $device
fi
grub-mkconfig -o /boot/grub/grub.cfg
reflector
systemctl enable --now fstrim.timer
systemctl enable acpid
systemctl enable avahi-daemon
systemctl enable cups
systemctl enable NetworkManager.service
systemctl enable --now systemd-timesyncreflector
