#!/bin/bash

read -p "Enter your UEFI-Partition (Type: EF00): /dev/" r_efiboot
read -p "Enter your root partition (Type: Linux File System): /dev/" r_root
read -p "Do you want to use an LTS-Kernel (type 1 for yes)? " lts
read -p "Enter your hostname: " hostname
read -p "Enter your password: " password
read -p "Enter your bootloader-id (Attentention this will overwrite bootloaders with the same id): " boot

./core/swap.sh

efiboot="/dev/$r_efiboot"
root="/dev/$r_root"

(echo "$root"; echo "1"; echo "$efiboot"; echo "$lts"; echo "$hostname"; echo "$password"; echo "$boot"; ) | ./core/base-install.sh

