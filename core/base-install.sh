#!/bin/bash

read root
read efi
read efiboot
read lts
read hostname
read password
read boot

mkfs.ext4 -L root $root
mount -L root /mnt

if [ "$efi" = "1" ]; then
  mkfs.fat -F 32 -n EFIBOOT $efiboot
  mkdir /mnt/boot
  mount -L EFIBOOT /mnt/boot
fi

reflector

if [ "$lts" = "1" ]; then
  (echo "y";) | pacstrap /mnt base base-devel linux-lts linux-firmware nano reflector xdg-user-dirs
else
  (echo "y";) | pacstrap /mnt base base-devel linux linux-firmware nano reflector xdg-user-dirs
fi

(echo "y";) | pacstrap /mnt dhcpcd bash-completion grub acpid dbus avahi cups networkmanager git

if [ "$efi" = "1" ]; then
  (echo "y";) | pacstrap /mnt efibootmgr
fi

genfstab -Up /mnt > /mnt/etc/fstab

echo $hostname > /mnt/etc/hostname
echo LANG=de_DE.UTF-8 > /mnt/etc/locale.conf
echo de_DE.UTF-8 UTF-8 > /mnt/etc/locale.gen
echo de_DE ISO-8859-1 >> /mnt/etc/locale.gen
echo de_DE@euro ISO-8859-15 >> /mnt/etc/locale.gen
echo en_US.UTF-8 UTF-8 >> /mnt/etc/locale.gen
echo KEYMAP=de-latin1 >> /mnt/etc/vconsole.conf
echo FONT=lat9w-16 >> /mnt/etc/vconsole.conf

cp core/root.sh /mnt/install.sh
chmod +x /mnt/install.sh

(echo "./install.sh"; echo "$efi"; echo "$efiboot"; echo "$password"; echo "$boot"; echo "exit"; ) | arch-chroot /mnt

rm /mnt/install.sh

umount /mnt

echo "finished base install"




