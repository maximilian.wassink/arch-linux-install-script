#!/bin/bash

read -p "Type 1 to continue with user setup: " user
if [ "$user" = "1" ]; then
  ./core/user.sh
fi

read -p "Type 1 to continue with AUR setup (user setup required): " aur
if [ "$aur" = "1" ]; then
  ./core/aur.sh
fi

read -p "Type 1 to continue with X setup: " x_server
if [ "$x_server" = "1" ]; then
  ./core/x_server.sh
fi

read -p "Type 1 to continue with font setup setup (user setup required): " font
if [ "$font" = "1" ]; then
  ./core/font.sh
fi
