#!/bin/bash

read -p "Enter your target device: /dev/" r_device
read -p "Enter your root partition (Type: Linux File System): /dev/$r_device" r_root
read -p "Do you want to use an LTS-Kernel (type 1 for yes)? " lts
read -p "Enter your hostname: " hostname
read -p "Enter your password: " password

./core/swap.sh

device="/dev/$r_device"
root="$device$r_root"

(echo "$root"; echo "0"; echo "$device"; echo "$lts"; echo "$hostname"; echo "$password"; echo "null"; ) | ./core/base-install.sh

