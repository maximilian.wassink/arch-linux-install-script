#!/bin/bash

function runnable() {
  chmod +x core/bios.sh
  chmod +x core/uefi.sh
  chmod +x core/base-install.sh
  chmod +x core/swap.sh
  chmod +x core/root.sh
  chmod +x extra/user.sh
  chmod +x extra/x_server.sh
  chmod +x extra/font.sh
}

echo "Starting Arch Installer by Maximilian Wassink"
echo "This is a german installer and the installer will install also german packages."
echo "making all files runnable..."
runnable
read -p "Type 1 for UEFI installation: " uefi
if [ "$uefi" = "1" ]; then
  ./core/uefi.sh
else
  ./core/bios.sh
fi

echo "Finished installation. There are more useful scripts in extra folder :) Please report any issues on gitlab."
