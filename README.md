# Arch Linux install-script

Basic Arch Linux install script

# List of gnome extensions
Caffeine
Clipboard Indicator
Dash to Dock
Desktop Icons NG
Screenshot Tool
Transparent Top Bar
User Themes

# Gnome themes
Applications: Arc-Darker-solid
Symbols: Arc 
Background: adapta-backgrounds (AUR)