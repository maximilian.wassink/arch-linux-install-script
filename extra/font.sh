#!/bin/bash

read -p "Enter your username: " username

mount -L root /mnt

(echo "y"; ) | pacstrap /mnt ttf-dejavu ttf-liberation ttf-linux-libertine

(echo "cd /tmp"; echo "su - $username"; echo "git clone https://aur.archlinux.org/ttf-ms-fonts.git"; echo "cd ttf-ms-fonts/"; echo "(echo \"j\"; ) | makepkg -si"; echo "exit"; ) | arch-chroot /mnt

umount /mnt

echo "installed ttf-dejavu, ttf-liberation, ttf-libertine and ttf-ms fonts"