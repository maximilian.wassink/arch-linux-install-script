#!/bin/bash

read -p "Enter username (1 lower case word): " username
read -p "Enter password: " password

mount -L root /mnt

(echo "y"; ) | pacstrap /mnt sudo
echo "%wheel ALL=(ALL) ALL" >> /mnt/etc/sudoers

(echo "useradd -m -g users -s /bin/bash $username"; echo "(echo \"$password\"; echo \"$password\"; ) | passwd $username"; echo "gpasswd -a $username wheel"; echo "xdg-user-dirs-update"; echo "passwd -l root"; echo "exit"; ) | arch-chroot /mnt

umount /mnt