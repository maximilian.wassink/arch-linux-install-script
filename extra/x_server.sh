#!/bin/bash

read -p "Enter your graphic driver (xorg-drivers for all drivers, nvidia for official nvidia, ...) " driver

mount -L root /mnt

(echo "y"; ) | pacstrap /mnt xorg-server xorg-xinit $driver xorg-xrandr

(echo "localectl --no-convert set-x11-keymap de pc105 deadgraveacute"; echo "exit"; ) | arch-chroot /mnt

umount /mnt